'use strict';

describe('Controller: FinderCtrl', function () {

    beforeEach(module('codenameSolid'));

    var MainCtrl,
        scope,
        model,
        http;

    beforeEach(inject(function ($controller, $rootScope) {
        scope = $rootScope.$new();
        model = {a: true};
        MainCtrl = $controller('FinderCtrl as ctrl', {
            $scope: scope
        });
    }));

    beforeEach(inject(function ($httpBackend) {
        http = $httpBackend;
    }));

    it('should set default search values to be empty', function () {
        console.log(scope.ctrl);
        expect(scope.ctrl.model.userSearchValue).toBe('');
        expect(scope.ctrl.model.noteSearchValue).toBe('');
    });

    it('should set default tab', function () {
        expect(scope.ctrl.model.tab).toBe('note');
    });

    it('should set search results empty by default', function () {
        expect(scope.ctrl.model.notes.length).toBe(0);
        expect(scope.ctrl.model.users.length).toBe(0);
    });

    it('should set favourites empty by default', function () {
        expect(scope.ctrl.model.favourites.length).toBe(0);
    });

    it('should set selected note and user false by default', function () {
        expect(scope.ctrl.model.activeNote).toBe(false);
        expect(scope.ctrl.model.activeUser).toBe(false);
    });

    it('should change tab', function () {
        expect(scope.ctrl.model.tab).toBe('note');
        scope.ctrl.changeTab('other');
        expect(scope.ctrl.model.tab).toBe('other');
    });
});
