(function () {
    'use strict';

    angular.module('codenameSolid')
        .config(function ($stateProvider, $urlRouterProvider) {
            // For any unmatched url, redirect to /state1
            $urlRouterProvider.otherwise('/login');

            // Now set up the states
            $stateProvider
                .state('login', {
                    url: '/login',
                    templateUrl: 'views/login.html',
                    controller: 'AuthCtrl',
                    controllerAs: 'AuthCtrl'
                })
                .state('logout', {
                    url: '/logout',
                    templateUrl: 'views/logout.html',
                    controller: 'AuthCtrl',
                    controllerAs: 'AuthCtrl'
                })
                .state('main', {
                    url: '/',
                    templateUrl: 'views/application-manager.html',
                    controller: 'AppManagerCtrl',
                    controllerAs: 'AppManagerCtrl'
                });
        });
}());

