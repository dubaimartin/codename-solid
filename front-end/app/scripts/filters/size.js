(function() {
    'use strict';

    angular.module('codenameSolid').filter('size', function() {

        return function(size, unit) {
            if(isNaN(size) || size < 1 && typeof unit !== 'string') {
                return size;
            } else {
                if(unit === 'KB') {
                    return (size / 1024).toFixed(2) + ' ' + unit;
                } else if(unit === 'MB') {
                    return (size / (1024*1024)).toFixed(2) + ' ' + unit;
                } else if(unit === 'GB') {
                    return (size / (1024*1024*1024)).toFixed(2) + ' ' + unit;
                }
            }
        }
    });
}());
