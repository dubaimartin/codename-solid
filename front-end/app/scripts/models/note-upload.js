(function () {
    'use strict';

    /**
     * Author: Dubai Martin
     */

    angular.module('codenameSolid').service('NoteUploadModel', function ($cookies) {
        var model = {
            note: {
                title: '',
                description: '',
                attachments: [],
                owner: '',
                ownerName: $cookies.get('activeUserName'),
                liked: [],
                disliked: [],
                privacy: 'public',
                sharedWith: [],
                created: '',
                modified: ''
            },

            tab: 'basic',
            files: [],
            uploadedFiles: [],
            uploading: false,

            reset: reset
        };

        function reset() {
            model.note = {
                title: '',
                description: '',
                attachments: [],
                owner: '',
                ownerName: $cookies.get('activeUserName'),
                liked: [],
                disliked: [],
                privacy: 'public',
                sharedWith: [],
                created: '',
                modified: ''
            };
            model.tab = 'basic';
            model.files = [];
            model.file = null;
            model.uploadedFiles = [];
            model.uploading = false;
            model.progressPercentage = 0;

            return model;
        }

        return model;
    });
}());
