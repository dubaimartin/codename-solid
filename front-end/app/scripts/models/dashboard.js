(function () {
    'use strict';

    /**
     * Author: Dubai Martin
     */

    angular.module('codenameSolid').service('DashboardModel', function () {
        var model = {
            news: [],
            following: [],

            reset: reset
        };

        function reset() {
            model.news = [];
            model.following = false;

            return model;
        }

        return model;
    });
}());
