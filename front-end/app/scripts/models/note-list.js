(function () {
    'use strict';

    /**
     * Author: Dubai Martin
     */

    angular.module('codenameSolid').service('NoteListModel', function () {
        var model = {
            notes: [],
            shared: [],
            tab: 'my',
            favourites: [],
            activeNote: null,

            reset: reset
        };

        function reset() {
            model.notes = [];
            model.shared = [];
            model.favourites = [];
            model.tab = 'my';
            model.activeNote = null;

            return model;
        }

        return model;
    });
}());

