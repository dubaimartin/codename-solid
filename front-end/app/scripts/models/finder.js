(function () {
    'use strict';

    /**
     * Author: Dubai Martin
     */

    angular.module('codenameSolid').service('FinderModel', function () {
        var model = {
            noteSearchValue: '',
            userSearchValue: '',
            tab: 'note',
            notes: [],
            users: [],
            favourites: [],
            activeNote: false,
            activeUser: false,
            followingCurrent: false,

            reset: reset
        };

        function reset() {
            model.noteSearchValue = '';
            model.userSearchValue = '';
            model.tab = 'note';
            model.notes = [];
            model.users = [];
            model.favourites = [];
            model.activeNote = false;
            model.activeUser = false;
            model.followingCurrent = false;

            return model;
        }

        return model;
    });
}());

