(function () {
    'use strict';

    /**
     * Author: Dubai Martin
     */

    angular.module('codenameSolid').service('ConversationModel', function () {
        var model = {
            conversations: [],
            notifications: [],
            tab: 'messages',
            polling: false,
            active: {
                currentMessage: '',
                conversation: false,
                messages: []
            },

            reset: reset
        };

        function reset() {
            model.polling = false;
            model.conversations = [];
            model.notifications = [];
            model.tab = 'messages';
            model.active.currentMessage = '';
            model.active.conversation = false;
            model.active.messages = [];

            return model;
        }

        return model;
    });
}());
