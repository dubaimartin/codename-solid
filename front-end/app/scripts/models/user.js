(function () {
    'use strict';

    /**
     * Author: Dubai Martin
     */

    angular.module('codenameSolid').service('UserModel', function () {
        var model = {
            user: {
                firstName: '',
                lastName: '',
                email: '',
                dateOfBirth: '',
                profilePicture: '',
                metadata: {
                    school: '',
                    country: '',
                    city: '',
                    interests: ''
                },
                following: [],
                followers: [],
                liked: [],
                disliked: [],
                conversations: [],
                notifications: [],
                favourites: []
            },
            followersList: [],
            followingList: [],
            mode: 'show',

            reset: reset
        };

        function reset() {
            model.user = false;
            model.mode = 'show';
            model.editing = false;
            model.followersList = [];
            model.followingList = [];

            return model;
        }

        return model;
    });
}());
