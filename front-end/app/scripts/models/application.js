(function () {

    'use strict';

    /**
     * Author: Dubai Martin
     */

    angular.module('codenameSolid').service('ApplicationModel', function () {
        var applications = [
            {
                id: 'dashboard',
                title: 'Dashboard',
                icon: 'fa fa-home',
                template: 'views/apps/dashboard.html',
                ctrl: 'DashboardCtrl',
                color: 'red',
                params: false
            },
            {
                id: 'profile',
                title: 'Profile',
                icon: 'fa fa-user',
                template: 'views/apps/profile.html',
                ctrl: 'ProfileCtrl',
                color: 'green',
                params: false
            },
            {
                id: 'upload',
                title: 'Note Manager',
                icon: 'fa fa-upload',
                template: 'views/apps/note-upload.html',
                ctrl: 'NoteUploadCtrl',
                color: 'orange',
                params: false
            },
            {
                id: 'notes',
                title: 'Your Notes',
                icon: 'fa fa-sticky-note',
                template: 'views/apps/note-list.html',
                ctrl: 'NoteListCtrl',
                color: 'purple',
                tab: 'my',
                params: false
            },
            {
                id: 'messages',
                title: 'Messages',
                icon: 'fa fa-comments',
                template: 'views/apps/messages.html',
                ctrl: 'MessagesCtrl',
                color: 'blue',
                params: false
            },
            {
                id: 'finder',
                title: 'Finder',
                icon: 'fa fa-search',
                template: 'views/apps/finder.html',
                ctrl: 'FinderCtrl',
                color: 'lightgreen',
                params: false
            }
        ];

        return applications;
    });
}());
