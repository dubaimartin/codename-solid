(function () {
    'use strict';

    /**
     * Author: Dubai Martin
     * @ngdoc function
     * @name frontEndApp.controller:NoteUploadCtrl
     * @description
     * # UploadCtrl
     * Controller of the frontEndApp
     */
    angular.module('codenameSolid').controller('NoteUploadCtrl',
        function ($rootScope, $scope, Common, ENV, Upload, $window, UserFunctions, $cookies, $http, NoteUploadModel, $mdDialog, $mdMedia, AppService, NoteService) {

            var ctrl = this;
            var model = NoteUploadModel;

            angular.extend(ctrl, {
                model: model,
                uploadedFiles: [],
                contacts: [],
                sharedWith: [],

                upload: upload,
                changeTab: changeTab,
                deleteFile: deleteFile,
                toggleEditing: toggleEditing,
                showFinalize: showFinalize,
                previewAttachment: previewAttachment,
                setPrivacy: setPrivacy
            });

            _init();

            $scope.$on('person-selected', function(event, args) {
                ctrl.contacts = args.contacts;
                _getSharedWith();
            });

            function showFinalize(ev) {
                var confirm = $mdDialog.confirm()
                    .title('Finalizing note')
                    .textContent('Please select whether you would like to finish your note.')
                    .targetEvent(ev)
                    .ok('Finalize')
                    .cancel('Cancel');
                $mdDialog.show(confirm).then(function () {
                    _finalizeNote();
                }, function () {
                });
            }

            function _finalizeNote() {
                _updateNote(ctrl.model.note);
                ctrl.model = NoteUploadModel.reset();
                AppService.setActiveApplication('notes');
            }

            function _init() {
                ctrl.model = NoteUploadModel;
                Common.setLoading();
                if (!ctrl.model.note._id) {
                    Common.setLoading();
                    ctrl.model.note.ownerName = $cookies.get('activeUserName');
                    return _saveNote(ctrl.model.note);
                }
                ctrl.sharedWith = ctrl.model.note.sharedWith;
                _getAttachments();
                if(ctrl.model.note.privacy === 'custom') {
                    _getSharedWith();
                }
                Common.setLoading();
            }

            function _getSharedWith() {
                UserFunctions.findUsersById(ctrl.model.note.sharedWith, function(result){
                    ctrl.contacts = result;
                });
            }

            function _updateNote(note) {
                NoteService.update(note);
                _getSharedWith();
            }

            function _getAttachments() {
                Common.setLoading();
                NoteService.getAttachments(ctrl.model.note._id, function (data) {
                    ctrl.uploadedFiles = data;
                    Common.setLoading();
                });
            }

            function _saveNote(note) {
                NoteService.save(note, function (data) {
                    ctrl.model.note = data;
                });
            }

            function changeTab(tab) {
                ctrl.model.tab = tab;
                if (tab === 'attachments') {
                    _getAttachments();
                }
            }

            function toggleEditing(idx) {
                ctrl.uploadedFiles[idx].editing = !ctrl.uploadedFiles[idx].editing;
                if (!ctrl.uploadedFiles[idx].editing) {
                    _updateFile(ctrl.uploadedFiles[idx]);
                }
            }

            function _updateFile(file) {
                NoteService.updateAttachment(file, function (res) {
                    if (res) {
                        _updateNote(ctrl.model.note);
                    }
                });
            }

            function deleteFile(id, idx) {
                NoteService.deleteAttachment(id, function (res) {
                    if (res) {
                        ctrl.uploadedFiles.splice(idx, 1);
                        ctrl.model.note.attachments.splice(idx, 1);
                        _updateNote(ctrl.model.note);
                    }
                });
            }

            function upload(files) {
                angular.forEach(files, function (_file, index) {
                    var idx = ctrl.uploadedFiles.length;
                    ctrl.uploadedFiles.push(_file);
                    ctrl.uploadedFiles[idx].uploading = true;
                    ctrl.uploadedFiles[idx].editing = false;
                    Upload.upload({
                        arrayKey: '',
                        url: ENV.apiEndpoint + '/note/upload/' + ctrl.model.note._id,
                        data: {
                            file: _file
                        }
                    }).then(function (file) {
                        ctrl.uploadedFiles[idx] = file.data;
                        ctrl.model.note.attachments.push(file.data.name);
                        _updateNote(ctrl.model.note);
                    }, function (resp) {
                    }, function (evt) {
                        ctrl.uploadedFiles[idx].progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    });
                });
            }

            function previewAttachment(file) {
                NoteService.previewAttachment(file);
            }

            function setPrivacy(privacy) {
                ctrl.model.note.privacy = privacy;

                if (privacy === 'custom') {
                    ctrl.model.note.sharedWith = ctrl.sharedWith;
                } else {
                    ctrl.model.note.sharedWith = [];
                }
            }
        });

    function DialogController($mdDialog, image, ENV) {
        var ctrl = this;

        ctrl.imagePath = ENV.apiEndpoint + '/get/' + image.uploader + '/' + image.name;
        ctrl.imageName = image.newName;

        ctrl.cancel = function () {
            $mdDialog.cancel();
        };
    }
}());
