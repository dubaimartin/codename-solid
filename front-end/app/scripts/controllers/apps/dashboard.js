(function () {
    'use strict';

    /**
     * Author: Dubai Martin
     * @ngdoc function
     * @name frontEndApp.controller:StartPageCtrl
     * @description
     * # StartPageCtrl
     * Controller of the frontEndApp
     */
    angular.module('codenameSolid').controller('DashboardCtrl',
        function (UserFunctions, $filter, Common, DashboardModel,$interval, AppService, NoteService) {

            var ctrl = this;
            var model = DashboardModel;

            angular.extend(ctrl, {
                model: model,
                interval: true,

                getDashboardNews: getDashboardNews,
                viewProfile: viewProfile,
                viewNote: viewNote
            });

            init();

            function init() {
                getDashboardNews();
                ctrl.interval = $interval(function() {
                    if(ctrl.model.following === false) {
                        $interval.cancel(ctrl.interval);
                    }
                    getDashboardNews();
                }, 30000);
            }

            function viewProfile(person) {
                AppService.setActiveApplication('finder', {person: person});
            }

            function viewNote(note) {
                AppService.setActiveApplication('notes', {note: note});
            }

            function getUserProfiles(ids) {
                UserFunctions.findUsersById(ids, function (res) {
                    angular.forEach(ctrl.model.news, function(value, key){
                        var obj = $filter('filter')(res, {_id: value.owner})[0];
                        value.ownerPersonObject = obj;
                    });
                });
            }

            function getDashboardNews() {
                NoteService.getDashboardNews(function (res) {
                    ctrl.model.news = res.notes;
                    ctrl.model.following = res.following;
                    getUserProfiles(ctrl.model.following);
                });
            }
        });
}());

