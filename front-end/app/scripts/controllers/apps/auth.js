(function () {
    'use strict';

    /**
     * Author: Dubai Martin
     * @ngdoc function
     * @name frontEndApp.controller:AuthCtrl
     * @description
     * # AuthCtrl
     * Controller of the frontEndApp
     */
    angular.module('codenameSolid')
        .controller('AuthCtrl', function ($scope, $rootScope, $http, $state, ENV, Notification, AppService, UserService, Common) {

            var ctrl = this;

            angular.extend(ctrl, {
                loginError: null,
                loading: false,
                registrationError: null,
                passwordType: 'password',
                loginUserData: {
                    password: '',
                    email: ''
                },
                registerUserData: {
                    firstName: '',
                    lastName: '',
                    email: '',
                    password: '',
                    dateOfBirth: new Date()
                },
                showRegistration: false,

                showRegistrationPanel: showRegistrationPanel,
                login: login,
                resetRegistrationForm: resetRegistrationForm,
                changePasswordType: changePasswordType,
                registerUser: registerUser
            });

            init();

            $rootScope.$on('sessionExpire', function(obj){
                console.log(obj);
                Notification.showCustomToast({
                    look: 'warning',
                    title: 'Session expired',
                    message: 'You have been logged out'
                });
            });

            function init() {
                UserService.isUserAuthenticated(function (res) {
                    if (!res) {
                        $state.go('login');
                    } else {
                        AppService.setActiveApplication('dashboard');
                        $state.go('main');
                    }
                });
            }

            function login(event) {
                if(event.keyCode === 13) {
                    ctrl.loading = Common.setLoading();
                    UserService.login(ctrl.loginUserData, function (res) {
                        ctrl.loading = Common.setLoading();
                    });
                }
            }

            function registerUser() {
                UserService.register(ctrl.registerUserData);
            }

            function showRegistrationPanel() {
                ctrl.showRegistration = !ctrl.showRegistration;
            }

            function resetRegistrationForm() {
                ctrl.registerUserData = {
                    firstName: '',
                    lastName: '',
                    email: '',
                    password: '',
                    dateOfBirth: ''
                };
            }

            function changePasswordType() {
                if (ctrl.passwordType === 'password') {
                    ctrl.passwordType = 'text';
                } else {
                    ctrl.passwordType = 'password';
                }
            }
        });

}());

