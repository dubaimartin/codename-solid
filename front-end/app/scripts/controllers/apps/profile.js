(function () {
    'use strict';

    /**
     * Author: Dubai Martin
     * @ngdoc function
     * @name frontEndApp.controller:ProfileCtrl
     * @description
     * # ProfileCtrl
     * Controller of the frontEndApp
     */
    angular.module('codenameSolid')
        .controller('ProfileCtrl', function ($cookies, AppService, UserFunctions, Common, Upload, ENV, UserModel) {

            var ctrl = this;
            var model = UserModel;

            angular.extend(ctrl, {
                model: model,

                init: init,
                changeMode: changeMode,
                upload: upload,
                viewProfile: viewProfile,
                unFollow: unFollow,
                update: update
            });

            init();

            function init() {
                Common.setLoading();
                getProfile(function () {
                    _getFollowers();
                    _getFollowing();
                    Common.setLoading();
                });
            }

            function unFollow(person) {
                UserFunctions.unFollow(person._id, function (res) {
                    ctrl.model.followingsList.splice(ctrl.model.followingsList.indexOf(person), 1);
                    ctrl.model.user.following.splice(ctrl.model.user.following.indexOf(person._id), 1);
                });
            }

            function update() {
                UserFunctions.update(ctrl.model.user, function (result) {
                });
            }

            function getProfile(callback) {
                UserFunctions.findUsersById([$cookies.get('activeUser')], function (res) {
                    ctrl.model.user = res[0];
                    if(callback) {
                        return callback();
                    }
                });
            }

            function _getFollowers() {
                UserFunctions.findUsersById(ctrl.model.user.followers, function (res) {
                    ctrl.model.followersList = res;
                });
            }

            function _getFollowing() {
                UserFunctions.findUsersById(ctrl.model.user.following, function (res) {
                    ctrl.model.followingsList = res;
                });
            }

            function viewProfile(person) {
                AppService.setActiveApplication('finder', {person: person});
            }

            function changeMode() {
                if (ctrl.model.mode === 'show') {

                    ctrl.model.mode = 'edit';
                } else {
                    ctrl.model.mode = 'show';
                }
            }

            function upload(file) {
                if(file) {
                    Common.setLoading();
                    Upload.upload({
                        arrayKey: '',
                        url: ENV.apiEndpoint + '/user/upload/profilePicture/',
                        data: {
                            file: file
                        }
                    }).then(function (res) {
                        console.log(res);
                        ctrl.model.user.profilePicture = res.data.url;
                        Common.setLoading();
                    }, function (resp) {
                    }, function (evt) {
                    });
                }
            }
        });
}());



