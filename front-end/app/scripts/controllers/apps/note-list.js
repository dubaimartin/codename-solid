(function () {
    'use strict';

    /**
     * Author: Dubai Martin
     * @ngdoc function
     * @name frontEndApp.controller:NoteListCtrl
     * @description
     * # NoteListCtrl
     * Controller of the frontEndApp
     */
    angular.module('codenameSolid').controller('NoteListCtrl',
        function ($scope, $rootScope, ENV, $http, NoteListModel, Common, NoteUploadModel, AppService, NoteService) {

            var ctrl = this;
            var model = NoteListModel;
            var noteModel = NoteUploadModel;

            angular.extend(ctrl, {
                model: model,
                noteModel: noteModel,

                edit: edit,
                remove: remove,
                changeTab: changeTab,
                open: open,
                showPrivacy: showPrivacy,
                downloadAttachment: downloadAttachment,
                previewAttachment: previewAttachment,
                removeFromFavourite: removeFromFavourite
            });

            function _init() {
                var params = AppService.getActiveApplication().params;
                if(params) {
                    if(params.note) {
                        open(params.note);
                    }
                } else {
                    Common.setLoading();
                    NoteService.get(function (data) {
                        ctrl.model.notes = data;
                        NoteService.getShared(function (data) {
                            ctrl.model.shared = data;
                            NoteService.getFavourites(function (data) {
                                ctrl.model.favourites = data;
                                Common.setLoading();
                            });
                        });
                    });
                }
            }

            _init();

            function showPrivacy(privacy, value) {
                if (value !== 'shared') {
                    return privacy === value;
                }
                return (angular.isArray(privacy) && privacy.length > 0);
            }

            function open(note) {
                ctrl.noteModel = note;
                NoteService.getAttachments(ctrl.noteModel._id, function (res) {
                    ctrl.noteModel.uploadedFiles = res;
                });
                changeTab('viewer');
            }

            function edit(note) {
                noteModel.note = note;
                AppService.setActiveApplication('upload');
            }

            function remove(index, note) {
                Common.setLoading();
                NoteService.remove(note._id, function (res) {
                    if (res) {
                        ctrl.model.notes.splice(index, 1);
                        ctrl.noteModel = noteModel.reset();
                    } else {
                    }
                    Common.setLoading();
                });
            }

            function downloadAttachment(file) {
                var fileName = file.newName;
                var a = document.createElement("a");
                document.body.appendChild(a);
                a.style = "display: none";
                NoteService.downloadAttachment(file.uploader, file.name).then(function (result) {
                    var downloadable = new Blob([result.data], {type: file.type});
                    var fileURL = window.URL.createObjectURL(downloadable);
                    a.href = fileURL;
                    a.download = fileName;
                    a.click();
                });
            }

            function removeFromFavourite(id) {
                NoteService.removeFromFavourites(id, function(res) {
                    ctrl.model.favourites.splice(ctrl.model.favourites.indexOf(id), 1);
                });
            }

            function changeTab(tab) {
                if (tab === 'shared') {
                    NoteService.getShared(function (data) {
                        ctrl.model.shared = data;
                    });
                }
                if (tab === 'favourites') {
                    NoteService.getFavourites(function (data) {
                        ctrl.model.favourites = data;
                    });
                }
                ctrl.model.tab = tab;
            }

            function previewAttachment(file) {
                NoteService.previewAttachment(file);
            }
        });
}());
