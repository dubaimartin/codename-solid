(function () {
    'use strict';

    /**
     * Author: Dubai Martin
     * @ngdoc function
     * @name frontEndApp.controller:MessagesCtrl
     * @description
     * # MessagesCtrl
     * Controller of the frontEndApp
     */
    angular.module('codenameSolid').controller('MessagesCtrl',
        function ($scope, $rootScope, Notification, Common, Conversation, ConversationModel, $cookies, $interval) {

            var ctrl = this;
            var model = ConversationModel;

            angular.extend(ctrl, {
                model: model,
                selectedContacts: [],
                interval: true,
                allInterval: true,

                changeTab: changeTab,
                selectConversation: selectConversation,
                sendMessage: sendMessage,
                getAll: getAll,
                checkIfMyMessage: checkIfMyMessage,
                closeConversation: closeConversation
            });

            function _init() {
                getAll();
                ctrl.allInterval = $interval(function() {
                    if(!ctrl.model.polling || !ctrl.model.active) {
                        $interval.cancel(ctrl.allInterval);
                    }
                    getAll();
                }, 5000);
            }

            function getAll() {
                Conversation.getAll(function(res) {
                    ctrl.model.conversations = res;
                    Notification.getAll(function(res) {
                        ctrl.model.notifications = res;
                    });
                });
            }

            function closeConversation() {
                $interval.cancel(ctrl.interval);
                ctrl.model.active = false;
                ctrl.model.polling = false;
            }
            _init();

            $scope.$on('createNewConversation', function(event, args) {
                var names = [];
                var profilePictures = [];
                angular.forEach(args.people, function(person, key) {
                    names.push(person.name);
                    profilePictures.push(person.profilePicture);
                });
                Conversation.create({
                    targets: args.sharedWith,
                    names: names,
                    profilePictures: profilePictures
                }, function(res) {
                    ctrl.model.conversations.push(res);
                });
                ctrl.selectedContacts = [];
            });

            function selectConversation(conversation) {
                $interval.cancel(ctrl.interval);
                get(conversation);
                    ctrl.interval = $interval(function() {
                        get(conversation);
                        ctrl.model.polling = true;
                    }, 10000);
            }

            function get(conversation) {
                Conversation.get(conversation, function(res, type) {
                    ctrl.model.active.conversation = angular.extend({}, res.conversation[0]);
                    ctrl.model.active.messages = angular.extend([], res.messages);
                })
            }

            function checkIfMyMessage(id) {
                return id == $cookies.get('activeUser');
            }

            function sendMessage(event) {
                if(event.keyCode === 13) {
                    Conversation.update(ctrl.model.active.conversation._id, {
                        message: ctrl.model.active.currentMessage,
                        name: $cookies.get('activeUserName')
                    }, function(res) {
                        ctrl.model.active.messages.push(res);
                        ctrl.model.active.currentMessage = '';
                    })
                }
            }

            function changeTab(tab) {
                ctrl.model.tab = tab;
                ctrl.getAll();
            }
        });

}());
