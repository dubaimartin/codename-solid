(function () {
    'use strict';

    /**
     * Author: Dubai Martin
     * @ngdoc function
     * @name frontEndApp.controller:NoteFinderCtrl
     * @description
     * # NoteFinderCtrl
     * Controller of the frontEndApp
     */
    angular.module('codenameSolid').controller('FinderCtrl',
        function (FinderModel, AppService, Common, NoteService, UserFunctions, $cookies) {

            var ctrl = this;
            var model = FinderModel;

            angular.extend(ctrl, {
                model: model,

                find: find,
                changeTab: changeTab,
                showPrivacy: showPrivacy,
                isFavourite: isFavourite,
                removeFromFavourite: removeFromFavourite,
                open: open,
                previewAttachment: previewAttachment,
                addToFavourite: addToFavourite,
                downloadAttachment: downloadAttachment,
                findUsers: findUsers,
                viewProfile: viewProfile,
                follow: follow,
                unFollow: unFollow
            });

            function _init() {
                _getFavourites();
                var params = AppService.getActiveApplication().params;
                if(params) {
                    if(params.person) {
                        ctrl.model.activeUser = params.person;
                        _checkFollow(ctrl.model.activeUser._id);
                        changeTab('profile');
                    }
                }
            }

            _init();

            function find() {
                if(ctrl.model.noteSearchValue.length >= 3) {
                    Common.setLoading();
                    NoteService.search(ctrl.model.noteSearchValue, function(data) {
                        ctrl.model.notes = data;
                        Common.setLoading();
                    });
                }
            }

            function viewProfile(user) {
                Common.setLoading();
                _checkFollow(user._id, function(result){
                    ctrl.model.activeUser = user;
                    changeTab('profile');
                    Common.setLoading();
                });
            }

            function findUsers() {
                UserFunctions.findUsers(ctrl.model.userSearchValue, function (result) {
                    ctrl.model.users = result;
                });
            }

            function follow(id) {
                UserFunctions.follow(id, function(res) {
                    _checkFollow(id);
                });
            }

            function unFollow(id) {
                UserFunctions.unFollow(id, function(res) {
                    _checkFollow(id);
                });
            }

            function _checkFollow(id, callback) {
                UserFunctions.checkFollow(id, function(result){
                    ctrl.model.followingCurrent = result.following;
                    if(callback) {
                        return callback(result);
                    }
                })
            }

            function showPrivacy(privacy, value) {
                if (value !== 'shared') {
                    return privacy === value;
                }
                return (angular.isArray(privacy) && privacy.length > 0);
            }

            function changeTab(tab) {
                ctrl.model.tab = tab;
            }

            function _getFavourites(){
                Common.setLoading();
                UserFunctions.findUsersById([ $cookies.get('activeUser') ], function(res) {
                    ctrl.model.favourites = res[0].favourites;
                    Common.setLoading();
                });
            }

            function removeFromFavourite(id) {
                NoteService.removeFromFavourites(id, function(res) {
                    ctrl.model.favourites.splice(ctrl.model.favourites.indexOf(id), 1);
                });
            }

            function addToFavourite(id) {
                NoteService.addToFavourites(id, function(res) {
                    ctrl.model.favourites.push(id);
                });
            }

            function open(note) {
                ctrl.model.activeNote = note;
                NoteService.getAttachments(ctrl.model.activeNote._id, function (res) {
                    ctrl.model.activeNote.uploadedFiles = res;
                });
                changeTab('viewer');
            }

            function isFavourite(id) {
                return ctrl.model.favourites.indexOf(id) > -1;
            }

            function previewAttachment(file) {
                NoteService.previewAttachment(file);
            }

            function downloadAttachment(file) {
                var fileName = file.newName;
                var a = document.createElement("a");
                document.body.appendChild(a);
                a.style = "display: none";
                NoteService.downloadAttachment(file.uploader, file.name).then(function (result) {
                    var downloadable = new Blob([result.data], {type: file.type});
                    var fileURL = window.URL.createObjectURL(downloadable);
                    a.href = fileURL;
                    a.download = fileName;
                    a.click();
                });
            }
        });
}());
