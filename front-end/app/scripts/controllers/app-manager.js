(function () {
    'use strict';

    /**
     * Author: Dubai Martin
     * @ngdoc function
     * @name frontEndApp.controller:AuthCtrl
     * @description
     * # AuthCtrl
     * Controller of the frontEndApp
     */
    angular.module('codenameSolid')
        .controller('AppManagerCtrl',
            function ($rootScope, $scope, $timeout, AppService, Notification, UserService, $state, $cookies, Common) {

            var ctrl = this;

            angular.extend(ctrl, {
                app: null,
                commonService: Common,
                loading: false,

                setActiveApp: setActiveApp,
                logout: logout
            });

            init();

            function init() {
                ctrl.app = AppService.getActiveApplication();
                ctrl.allApps = AppService.getApplications();

                ctrl.username = $cookies.get('activeUserName');
                UserService.isUserAuthenticated(function (res) {
                    if (!res) {
                        if ($state.get !== 'login') {
                            $state.go('login');
                        }
                    } else {
                        ctrl.isAuthenticated = true;
                        ctrl.app = AppService.setActiveApplication('dashboard');
                        Notification.pollNotifications();
                    }
                });
            }

            $rootScope.$on('appChanged', function(event, data) {
                ctrl.app = data.activeApplication;
            });

            function setActiveApp(id) {
                ctrl.app = AppService.setActiveApplication(id);
            }

            function logout() {
                UserService.logout();
            }
        });
}());


