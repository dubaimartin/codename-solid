(function () {
    'use strict';

    /**
     * Author: Dubai Martin
     * @ngdoc function
     * @name frontEndApp.directive:likes
     * @description
     * # display likes, dislikes
     * Directive of the frontEndApp
     */

    angular.module('codenameSolid').directive('likes', function (Like) {
        var menu = {
            restrict: 'E',
            templateUrl: 'views/directives/likes.html',
            scope: {},
            bindToController: {
                note: '='
            },
            controller: function() {
                var ctrl = this;

                ctrl.like = function() {
                    Like.like(ctrl.note._id, function(res) {
                        if(res.action === 'add') {
                            ctrl.note.liked.push(res.userid);
                            if(ctrl.note.disliked.indexOf(res.userid) > -1) {
                                ctrl.note.disliked.splice(ctrl.note.disliked.indexOf(res.userid, 1));
                            }
                        } else if(res.action === 'remove') {
                            ctrl.note.liked.splice(ctrl.note.liked.indexOf(res.userid, 1));
                        }
                    });
                };

                ctrl.dislike = function() {
                    Like.dislike(ctrl.note._id, function(res) {
                        if(res.action === 'add') {
                            ctrl.note.disliked.push(res.userid);
                            if(ctrl.note.liked.indexOf(res.userid) > -1) {
                                ctrl.note.liked.splice(ctrl.note.liked.indexOf(res.userid, 1));
                            }
                        } else if(res.action === 'remove') {
                            ctrl.note.disliked.splice(ctrl.note.disliked.indexOf(res.userid, 1));
                        }
                    });
                };
            },
            controllerAs: 'ctrl'
        };

        return menu;
    });
}());
