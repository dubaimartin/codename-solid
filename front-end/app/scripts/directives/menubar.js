(function () {
    'use strict';

    /**
     * Author: Dubai Martin
     * @ngdoc function
     * @name frontEndApp.directive:menuBar
     * @description
     * # menuBar directive to display the top menu bar
     * Directive of the frontEndApp
     */

    angular.module('codenameSolid').directive('menuBar', function () {
        var menu = {
            restrict: 'E',
            templateUrl: 'views/menubar.html'
        };

        return menu;
    });
}());

