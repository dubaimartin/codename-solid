(function () {
    'use strict';

    /**
     * Author: Dubai Martin
     * @ngdoc function
     * @name frontEndApp.directive: user-finder
     * @description
     * # display likes, dislikes
     * Directive of the frontEndApp
     */

    angular.module('codenameSolid').directive('userFinder', function () {
        return {
            restrict: 'E',
            templateUrl: 'views/directives/user-finder.html',
            scope: {},
            transclude: true,
            bindToController: {
                sharedWith: '=',
                contacts: '=',
                type: '='
            },
            controller: UserFinderController,
            controllerAs: 'ctrl'
        };
    });

    function UserFinderController($rootScope, $mdMedia, $mdDialog, $scope) {
        var ctrl = this;

        ctrl.people = [];
        ctrl.showAdvanced = function (ev) {
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
            $mdDialog.show({
                controller: DialogController,
                controllerAs: 'DialogController',
                templateUrl: 'views/common/people-finder.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: useFullScreen,
                locals: {
                    people: ctrl.people,
                    sharedWith: ctrl.sharedWith,
                    type: ctrl.type
                }
            })
            $scope.$watch(function () {
                return $mdMedia('xs') || $mdMedia('sm');
            }, function (wantsFullScreen) {
                $scope.customFullscreen = (wantsFullScreen === true);
            });
        };

        $scope.$on('person-selected', function (event, args) {
            ctrl.sharedWith = args.sharedWith;
            ctrl.contacts = args.contacts;
        });
    }

    function DialogController($rootScope, $mdDialog, $cookies, people, sharedWith, type, UserFunctions, Notification) {
        var ctrl = this;
        var selectedPeople = [];

        ctrl.people = people;
        ctrl.searchedString = '';
        ctrl.sharedWith = sharedWith;
        ctrl.type = type;

        var broadcastUserIdsAccept = [];
        var broadcastUserIdsDecline = [];

        ctrl.close = function () {
            if (broadcastUserIdsAccept.length > 0 && ctrl.type === 'noteShare') {
                Notification.sendNoteShareNotification(assembleShareQuery(broadcastUserIdsAccept, 'noteShare'));
            }
            if (broadcastUserIdsDecline.length > 0 && ctrl.type === 'noteShare') {
                Notification.sendNoteShareNotification(assembleShareQuery(broadcastUserIdsDecline, 'noteShareRemove'));
            }
            if (ctrl.sharedWith.length > 0 && ctrl.type === 'conversation') {
                $rootScope.$broadcast('createNewConversation', {sharedWith: ctrl.sharedWith, people: selectedPeople});
                Notification.sendConversationNotification(assembleShareQuery(broadcastUserIdsAccept, 'conversation'));
                ctrl.sharedWith = [];
            }
            $mdDialog.hide();
        };

        ctrl.selectPerson = function (index, person) {
            var id = ctrl.people[index]._id;
            var checked = !ctrl.people[index].checked;
            ctrl.people[index].checked = checked;
            if (ctrl.sharedWith.indexOf(id) > -1) {
                ctrl.sharedWith.splice(ctrl.sharedWith.indexOf(id), 1);
                selectedPeople.splice(ctrl.sharedWith.indexOf(id), 1);
            } else {
                ctrl.sharedWith.push(id);
                selectedPeople.push({
                    profilePicture: person.profilePicture,
                    name: person.firstName + ' ' + person.lastName
                });
            }

            manageBroadcastUserIds(checked, id);
            $rootScope.$broadcast('person-selected', {sharedWith: ctrl.sharedWith, contacts: ctrl.people});
        };

        function manageBroadcastUserIds(checked, id) {
            if (checked && broadcastUserIdsAccept.indexOf(id) < 0) {
                broadcastUserIdsAccept.push(id);
                broadcastUserIdsDecline.splice(broadcastUserIdsDecline.indexOf(id), 1);
            } else {
                broadcastUserIdsAccept.splice(broadcastUserIdsAccept.indexOf(id), 1);
                broadcastUserIdsDecline.push(id);
            }
        }

        ctrl.queryUsers = function (value) {
            UserFunctions.findUsers(value, function (result) {
                ctrl.people = result;
                angular.forEach(ctrl.people, function (value, key) {
                    if (ctrl.sharedWith.indexOf(value._id) > -1) {
                        value.checked = true;
                    } else {
                        value.checked = false;
                    }
                });
            });
        };

        function assembleShareQuery(ids, type) {
            return {
                userIds: ids,
                type: type,
                username: $cookies.get('activeUserName'),
                userId: $cookies.get('activeUser')
            };
        }
    }
}());
