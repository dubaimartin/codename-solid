(function () {
    'use strict';

    /**
     * Author: Dubai Martin
     * @ngdoc function
     * @name frontEndApp.directive:fileTypeIcon
     * @description
     * # display likes, dislikes
     * Directive of the frontEndApp
     */

    angular.module('codenameSolid').directive('fileTypeIcon', function () {
        var directive = {
            restrict: 'E',
            templateUrl: 'views/directives/file-type-icon.html',
            scope: {
                type: '='
            },
            link: function(scope, element, attrs) {
            }
        };

        return directive;
    });
}());
