(function () {
    'use strict';

    /**
     * Author: Dubai Martin
     * @ngdoc overview
     * @name frontEndApp
     * @description
     * # frontEndApp
     *
     * Main module of the application.
     */
    angular
        .module('codenameSolid', [
            'ngAnimate',
            'ngCookies',
            'ngMessages',
            'ngResource',
            'ngRoute',
            'ngSanitize',
            'ngTouch',
            'ngMaterial',
            'ui.router',
            'ngCookies',
            'ngFileUpload',
            'toastr'
        ]);
}());


