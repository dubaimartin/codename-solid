(function () {
    'use strict';

    /**
     * Author: Dubai Martin
     */

    angular.module('codenameSolid').service('Notification', function ($http, $rootScope, ENV, $state, toastr, $interval) {

        var interval = false;

        var service = {
            interval: interval,

            getAll: getAll,
            sendNoteShareNotification: sendNoteShareNotification,
            pollNotifications: pollNotifications,
            showCustomToast: showCustomToast,
            sendConversationNotification: sendConversationNotification,
            stopPolling: stopPolling
        };

        function getAll(callback) {
            $http.get(ENV.apiEndpoint + '/notifications')
                .success(function(res) {
                    if(callback) {
                        callback(res);
                    }
                })
        }

        function sendNoteShareNotification(data, callback) {
            $http.post(ENV.apiEndpoint + '/notifications/note/share', data)
                .success(function (res) {
                })
        }

        function sendConversationNotification(data, callback) {
            $http.post(ENV.apiEndpoint + '/notifications/conversation', data)
                .success(function (res) {
                })
        }

        function pollNotifications() {
            service.interval = $interval(function () {
                $http({
                    method: 'GET',
                    url: ENV.apiEndpoint + '/notifications/new',
                    withCredentials: true
                })
                    .success(function (res) {
                        angular.forEach(res, function (value, key) {
                            if(!value.sent) {
                                showCustomToast(value);
                                sentNotification(value);
                            }
                        });
                    })
                    .error(function(res) {
                        $interval.cancel(service.interval);
                        $rootScope.$broadcast('sessionExpire');
                        $state.go('login');
                    })
            }, 5000);
        }

        function stopPolling() {
            $interval.cancel(service.interval);
        }

        function sentNotification(data) {
            $http({
                method: 'PUT',
                url: ENV.apiEndpoint + '/notifications/' + data._id + '/sent',
                withCredentials: true
            })
                .success(function (res) {
                })
        }

        function showCustomToast(data) {
            if(data.look === 'success') {
                toastr.success(data.message, data.title);
            } else if(data.look === 'error') {
                toastr.error(data.message, data.title);
            } else if(data.look === 'warning') {
                toastr.warning(data.message, data.title);
            } else {
                toastr.info(data.message, data.title);
            }
        }

        return service;
    });
}());

