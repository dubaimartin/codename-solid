(function () {
    'use strict';

    /**
     * Author: Dubai Martin
     */

    angular.module('codenameSolid').service('NoteService', function ($http, $rootScope, $state, $window, $mdDialog, ENV, Notification) {
        var service = {

            getDashboardNews: function (callback) {
                $http.get(ENV.apiEndpoint + '/notes/dashboard')
                    .success(function (data) {
                        if (callback) {
                            callback(data);
                        }
                    })
                    .error(function(res) {
                        $rootScope.$broadcast('sessionExpire');
                        $state.go('login');
                    })
            },

            get: function (callback) {
                $http.get(ENV.apiEndpoint + '/notes')
                    .success(function (data) {
                        if (callback) {
                            callback(data);
                        }
                    })
                    .error(function (err) {
                        if (callback) {
                            callback(err);
                        }
                    })
            },

            addToFavourites: function (id, callback) {
                $http.put(ENV.apiEndpoint + '/note/' + id + '/favourite/add')
                    .success(function (data) {
                        if (callback) {
                            callback(data);
                        }
                        Notification.showCustomToast({
                            title: 'Success',
                            message: 'Note added to favourites!',
                            look: 'success'
                        });
                    })
                    .error(function (err) {
                        if (callback) {
                            callback(err);
                        }
                    })
            },

            removeFromFavourites: function (id, callback) {
                $http.delete(ENV.apiEndpoint + '/note/' + id + '/favourite/remove')
                    .success(function (data) {
                        if (callback) {
                            callback(data);
                        }
                        Notification.showCustomToast({
                            title: 'Success',
                            message: 'Note removed from favourites!',
                            look: 'info'
                        });
                    })
                    .error(function (err) {
                        if (callback) {
                            callback(err);
                        }
                    })
            },

            search: function (name, callback) {
                $http.get(ENV.apiEndpoint + '/notes/' + name)
                    .success(function (data) {
                        if (callback) {
                            callback(data);
                        }
                    })
                    .error(function (err) {
                        if (callback) {
                            callback(err);
                        }
                    })
            },

            getShared: function (callback) {
                $http.get(ENV.apiEndpoint + '/notes/shared')
                    .success(function (data) {
                        if (callback) {
                            callback(data);
                        }
                    })
                    .error(function (err) {
                        if (callback) {
                            callback(err);
                        }
                    })
            },

            update: function (note) {
                $http.put(ENV.apiEndpoint + '/note/' + note._id, note)
                    .success(function (res) {
                    })
                    .error(function (res) {
                    })
            },

            save: function (note, callback) {
                $http.post(ENV.apiEndpoint + '/note/save', note)
                    .success(function (res) {
                        if (callback) {
                            Notification.showCustomToast({
                                title: 'Success',
                                message: 'Note saved',
                                look: 'success'
                            });
                            callback(res);
                        }
                    })
                    .error(function (res) {
                        if (callback) {
                            Notification.showCustomToast({
                                title: 'Error',
                                message: 'Error while saving Note!',
                                look: 'error'
                            });
                            callback(res);
                        }
                    })
            },

            remove: function (id, callback) {
                $http.delete(ENV.apiEndpoint + '/note/' + id)
                    .success(function (res) {
                        if (callback) {
                            Notification.showCustomToast({
                                title: 'Success',
                                message: 'Note deleted!',
                                look: 'success'
                            });
                            callback(true);
                        }
                    })
                    .error(function (res) {
                        Notification.showCustomToast({
                            title: 'Error',
                            message: 'Error while deleting Note!',
                            look: 'error'
                        });
                        if (callback) {
                            callback(false);
                        }
                    })
            },

            getFavourites: function (callback) {
                $http.get(ENV.apiEndpoint + '/note/favourites')
                    .success(function (res) {
                        if (callback) {
                            callback(res);
                        }
                    })
                    .error(function (err) {
                        if (callback) {
                            callback(false);
                        }
                    })
            },

            getAttachments: function (id, callback) {
                $http.get(ENV.apiEndpoint + '/note/' + id + '/attachments')
                    .success(function (res) {
                        if (callback) {
                            callback(res);
                        }
                    })
                    .error(function (err) {
                        if (callback) {
                            callback(false);
                        }
                    })
            },

            deleteAttachment: function (id, callback) {
                $http.delete(ENV.apiEndpoint + '/note/upload/' + id)
                    .success(function (res) {
                        if (callback) {
                            callback(true);
                        }
                    })
                    .error(function (res) {
                        if (callback) {
                            callback(false);
                        }
                    })
            },

            updateAttachment: function (file, callback) {
                $http.put(ENV.apiEndpoint + '/note/upload/' + file.id, {
                    newName: file.newName
                })
                    .success(function (res) {
                        if (callback) {
                            callback(true);
                        }
                    })
                    .error(function (res) {
                        if (callback) {
                            callback(false);
                        }
                    })
            },

            downloadAttachment: function (uploader, name) {
                return $http({
                    method: 'GET',
                    url: ENV.apiEndpoint + '/get/' + uploader + '/' + name,
                    withCredentials: true,
                    responseType: 'arraybuffer'
                }).then(function (response) {
                    return response;
                })
            },

            previewAttachment: function (file) {
                if (file.type.indexOf('image') === 0) {
                    $mdDialog.show({
                        controller: DialogController,
                        controllerAs: 'DialogController',
                        templateUrl: 'views/common/image-preview-dialog.html',
                        parent: angular.element(document.body),
                        clickOutsideToClose: true,
                        fullscreen: true,
                        locals: {
                            image: file
                        }
                    });
                } else if (file.type.includes('pdf')) {
                    var filePath = ENV.apiEndpoint + '/get/pdf/' + file.uploader + '/' + file.name;
                    $window.open(filePath);
                }
            }
        };

        return service;
    });

    function DialogController($mdDialog, image, ENV) {
        var ctrl = this;

        ctrl.imagePath = ENV.apiEndpoint + '/get/' + image.uploader + '/' + image.name;
        ctrl.imageName = image.newName;

        ctrl.cancel = function () {
            $mdDialog.cancel();
        };
    }
}());

