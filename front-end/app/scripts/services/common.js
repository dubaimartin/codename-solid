(function(){
    'use strict';

    /**
     * Author: Dubai Martin
     */

    angular.module('codenameSolid').service('Common',
        function(NoteUploadModel, NoteListModel, DashboardModel, ConversationModel, UserModel, FinderModel) {

        var service = {
            getLoading: getLoading,
            setLoading: setLoading,
            resetModels: resetModels
        };

        var loading = false;

        function getLoading() {
            return loading;
        }

        function setLoading() {
            loading = !loading;
            return loading;
        }

        function resetModels() {
            NoteUploadModel.reset();
            NoteListModel.reset();
            ConversationModel.reset();
            UserModel.reset();
            FinderModel.reset();
            DashboardModel.reset();
        }

        return service;
    });
}());

