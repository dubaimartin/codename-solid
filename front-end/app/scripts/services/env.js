(function () {
    'use strict';

    /**
     * Author: Dubai Martin
     */

    angular.module('codenameSolid').service('ENV', function () {
        var service = {
            apiEndpoint: 'http://localhost:3333'
        };

        return service;
    });
}());

