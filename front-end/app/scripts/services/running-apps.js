(function () {
    'use strict';

    /**
     * Author: Dubai Martin
     */

    angular.module('codenameSolid').service('AppService', function (ApplicationModel, $rootScope) {
        var runningApplications = [];
        var activeApplication = false;
        var applications = ApplicationModel;

        var service = {
            addRunningApplication: function (app) {
                runningApplications.push(app);
            },

            getRunningApplication: function (app) {
                return runningApplications.indexOf(app);
            },

            getApplications: function () {
                return applications;
            },

            setActiveApplication: function (id, params) {
                for (var i = 0; i < applications.length; i++) {
                    if (applications[i].id === id) {
                        activeApplication = applications[i];

                        ApplicationModel[i].params = params;
                        $rootScope.$broadcast('appChanged', {activeApplication: activeApplication});
                        return activeApplication;
                    }
                }
            },

            getActiveApplication: function () {
                return activeApplication;
            }
        };

        return service;
    });

}());
