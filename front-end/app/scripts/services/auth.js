(function () {
    'use strict';

    /**
     * Author: Dubai Martin
     */

    angular.module('codenameSolid').service('UserService',
        function ($http, ENV, $state, $location, $cookies, Common, AppService, Notification) {

            var service = {
                setUser: function (data) {
                    $cookies.put('activeUser', data.user._id);
                    $cookies.put('activeUserName', data.user.firstName + ' ' + data.user.lastName);
                },

                isUserAuthenticated: function (callback) {
                    $http({
                        method: 'GET',
                        url: ENV.apiEndpoint + '/login',
                        withCredentials: true
                    })
                        .success(function (data) {
                            if (callback) {
                                callback(true);
                            }
                        })
                        .error(function (data) {
                            if (callback) {
                                callback(false);
                            }
                        })
                    ;
                },

                logout: function () {
                    $http({
                        method: 'GET',
                        url: ENV.apiEndpoint + '/logout',
                        withCredentials: true
                    })
                        .success(function () {
                            $cookies.remove('activeUser');
                            $cookies.remove('activeUserName');

                            Common.resetModels();
                            Notification.stopPolling();

                            $state.go('login');
                        })
                        .error(function (data) {
                        });
                },

                login: function (user, callback) {
                    $http({
                        method: 'POST',
                        url: ENV.apiEndpoint + '/login',
                        withCredentials: true,
                        data: user
                    })
                        .success(function (data) {
                            AppService.setActiveApplication('dashboard');
                            service.setUser(data);
                            $state.go('main');
                            Notification.showCustomToast({
                                title: 'Info',
                                message: 'Successfully logged in!',
                                look: 'info'
                            });
                            if (callback) {
                                callback(data);
                            }
                        })
                        .error(function (err) {
                            if(!err.message) {
                                err.message = 'Error';
                            }
                            if (callback) {
                                Notification.showCustomToast({
                                    title: 'Could not log in!',
                                    message: 'Error: ' + err.message,
                                    look: 'info'
                                });
                                callback(err);
                            }
                        });
                },

                register: function (user) {
                    $http.post(ENV.apiEndpoint + '/register', user)
                        .success(function (data) {
                            Notification.showCustomToast({
                                title: 'Info',
                                message: 'Registration successful!',
                                look: 'success'
                            });
                            var registrationError = {
                                error: false,
                                value: 'Registration successful, you can now login!'
                            };
                            return registrationError;
                        })
                        .error(function (err) {
                            if(!err) {
                                err = 'Error';
                            }
                            Notification.showCustomToast({
                                title: 'Error while registration',
                                message: 'Error: ' + err,
                                look: 'warning'
                            });
                            var registrationError = {
                                error: true,
                                value: 'Error: ' + err
                            };
                            return registrationError;
                        });
                }
            };

            return service;
        });

}());
