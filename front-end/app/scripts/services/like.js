(function(){
    'use strict';

    /**
     * Author: Dubai Martin
     */

    angular.module('codenameSolid').service('Like', function(ENV, $http){
        var service = {
            like: like,
            dislike: dislike
        };

        var loading = false;

        function like(id, callback) {
            $http.put(ENV.apiEndpoint + '/note/' + id + '/like')
                .success(function(res){
                    if(callback) {
                        callback(res);
                    }
                })
                .error(function (err) {
                    if (callback) {
                        callback(err);
                    }
                })
        }

        function dislike(id, callback) {
            $http.put(ENV.apiEndpoint + '/note/' + id + '/dislike')
                .success(function(res){
                    if(callback) {
                        callback(res);
                    }
                })
                .error(function (err) {
                    if (callback) {
                        callback(err);
                    }
                })
        }

        return service;
    });
}());

