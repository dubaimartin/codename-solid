(function () {
    'use strict';

    /**
     * Author: Dubai Martin
     */

    angular.module('codenameSolid').service('UserFunctions', function ($http, ENV) {
        var service = {
            findUsers: findUsers,
            findUsersById: findUsersById,
            getUsername: getUsername,
            checkFollow: checkFollow,
            follow: follow,
            unFollow: unFollow,
            update: update
        };

        function findUsers(value, callback) {
            if (value.length >= 3) {
                $http.get(ENV.apiEndpoint + '/users/find/' + value)
                    .success(function (res) {
                        if (callback) {
                            callback(res);
                        }
                    })
                    .error(function(res) {
                        if (callback) {
                            callback(res);
                        }
                    })
            }
        }

        function update(person, callback) {
            $http.put(ENV.apiEndpoint + '/user/update', {person: person})
                .success(function (res) {
                    if (callback) {
                        callback(res);
                    }
                })
                .error(function(res) {
                    if (callback) {
                        callback(res);
                    }
                })
        }

        function getUsername(value, callback) {
            $http.get(ENV.apiEndpoint + '/users/find/username/' + value)
                .success(function (res) {
                    if (callback) {
                        callback(res);
                    }
                })
                .error(function(res) {
                    if (callback) {
                        callback(res);
                    }
                })
        }

        function findUsersById(ids, callback) {
            $http.post(ENV.apiEndpoint + '/users/find/id', { ids: ids })
                .success(function (res) {
                    if (callback) {
                        callback(res);
                    }
                })
                .error(function(res) {
                    if (callback) {
                        callback(res);
                    }
                })
        }

        function checkFollow(id, callback) {
            $http.get(ENV.apiEndpoint + '/users/' + id + '/checkfollow')
                .success(function (res) {
                    if (callback) {
                        callback(res);
                    }
                })
                .error(function(res) {
                    if (callback) {
                        callback(res);
                    }
                })
        }

        function follow(id, callback) {
            $http.put(ENV.apiEndpoint + '/users/' + id + '/follow')
                .success(function (res) {
                    if (callback) {
                        callback(res);
                    }
                })
                .error(function(res) {
                    if (callback) {
                        callback(res);
                    }
                })
        }

        function unFollow(id, callback) {
            $http.put(ENV.apiEndpoint + '/users/' + id + '/unfollow')
                .success(function (res) {
                    if (callback) {
                        callback(res);
                    }
                })
                .error(function(res) {
                    if (callback) {
                        callback(res);
                    }
                })
        }

        return service;
    });
}());

