(function(){
    'use strict';

    /**
     * Author: Dubai Martin
     */

    angular.module('codenameSolid').service('Conversation', function($http, $rootScope, $state, ENV, UserService, Notification){
        var service = {
            create: create,
            getAll: getAll,
            get: get,
            update: update
        };

        function create(data, callback) {
            $http.post(ENV.apiEndpoint + '/conversation/new', data)
                .success(function(res) {
                    Notification.showCustomToast({
                        title: 'Success',
                        message: 'Created a new conversation!',
                        look: 'success'
                    });
                    if(callback) {
                        callback(res);
                    }
                })
                .error(function (data) {
                    Notification.showCustomToast({
                        title: 'Info',
                        message: 'Error while creating new conversation!',
                        look: 'warning'
                    });
                    if(callback) {
                        callback(data);
                    }
                });
        }

        function getAll(callback) {
            $http.get(ENV.apiEndpoint + '/conversation/getAll')
                .success(function(res) {
                    if(callback) {
                        callback(res);
                    }
                })
                .error(function(res) {
                    $rootScope.$broadcast('sessionExpire');
                    $state.go('login');
                })
        }

        function get(data, callback) {
            $http.get(ENV.apiEndpoint + '/conversation/' + data._id + '/get')
                .success(function(res) {
                    if (callback) {
                        callback(res, 'success');
                    }
                })
                .error(function(res) {
                    $rootScope.$broadcast('sessionExpire');
                    $state.go('login');
                })
        }

        function update(id, data, callback) {
            $http.post(ENV.apiEndpoint + '/conversation/' + id + '/update', data)
                .success(function(res) {
                    if(callback) {
                        callback(res);
                    }
                })
        }

        return service;
    });
}());

