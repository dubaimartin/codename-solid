# codename-solid

BSc Thesis - Author: Dubai Martin

## Installing (on linux)

* `sudo add-apt-repository ppa:chris-lea/node.js^C`
* `sudo apt-get update`
* `sudo apt-get install nodejs`
* `npm install`
* `sudo npm install -g generator-angular`
* `sudo npm install -g grunt-cli`
* `sudo apt-get install ruby-full`
* `sudo npm install bower -g`

* `sudo gem install compass`
* `sudo gem install sass`

## Running project

* Run `grunt serve` for running FE in the front-end folder
* Run `npm start` for running BE in the rest-server folder
* Run `mongod` for starting MongoDB service in Windows

## Testing

Running `grunt test` will run the unit tests with karma.

## Schemas ##

### User ###

```
{
	firstName: string,
	lastName: string,
	email: string,
	password: string,
	dateOfBirth: date (TODO),
	metadata: {
		school: string,
		country: string',
		town: string,
		interests: string
	},
	following: [userid:string],
	followers: [userid:string],
	liked: [userid:string],
	disliked: [userid:string],
        conversations: [conversation ids],
        notifications: [notification ids]
}
```

### Notification ### TODO
#### Types ####
Server: login, logout, server errors... (no action needed, just notification)

Message: Incoming messages from users (view msg interaction)

Interaction: New like/dislike, new share, new follow (view action interaction)

```
{
   type: 'message/interaction',
   title: 'Title',
   message: 'Message',
   - if message | interaction variable is set -
   actions: 'new shared note, likes, dislikes', 'new follower', 'new message',
   - if message is set -
   messageDetails: {
        conversationId: coming from already working conversation,
        _id: 'ID - generate here',
        source: 'userid1',
        target: ['myuserid', 'myuserid2'],
        short: 'my message short...',
        long: 'my message short, but longer, whole text',
        sent: DATE
   },
   read: false/true
}
```

# Message #
```
{
        conversationId: coming from already working conversation,
        _id: 'ID - generate here',
        source: 'userid1',
        target: ['myuserid', 'myuserid2'],
        short: 'my message short...',
        long: 'my message short, but longer, whole text',
        sent: DATE
}
```

### Conversation ###
```
{
       _id: 'id1',
       owner: 'profileid',
       targets: ['profileid1', 'profileid2'],
       messages: ['messageid1', 'messageid2'],
       created: DATE,
       updated: DATE
}
```

### Notes ###
TODO:
attachments drag-n-drop reordering, renaming, deleting, privacy (later!)
```
{
	title: string,
	description: string,
	owner: string,
	attachments: [File],
	liked: [string:userid],
	disliked: [string:userid],
	privacy: private/public/[userids:string],
        created: string,
        modified: string
}
```

### Attachment files ###
```
{
	id: string,
	location: string (? optional),
	name: string,
	type: string,
	privacy: private/public/[userids:string] (? optional),
        created: string,
        modified: string
}
```

### Dashboard ### TODO
```
{
	userid: string,
	savedApps: [
		{
			title: string,
			order: 0
		},
		{
			title: string,
			order: 1		
		}
	],
	settings: {
		showHints: boolean,
		showOnStart: boolean,
		backgroundImage: string
	}
}
```