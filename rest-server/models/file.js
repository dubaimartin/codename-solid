'use strict';

/**
 * Author: Dubai Martin
 * File attachment model
 */

var mongoose = require('mongoose');
var path = require('path');

var FileSchema = mongoose.Schema({
    uploader: String,
    noteid: String,
    name: String,
    newName: String,
    type: String,
    destination: String,
    path: String,
    privacy: String,
    size: Number,
    created: { type : Date, default: Date.now },
    lastModifiedDate: { type : Date, default: Date.now }
});
module.exports = mongoose.model('File', FileSchema);