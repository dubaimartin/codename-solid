'use strict';

/**
 * Author: Dubai Martin
 * Notification model
 */

var mongoose = require('mongoose');

var NotificationSchema = mongoose.Schema({
    owner: String,
    type: String,
    title: String,
    message: String,
    action: String,
    read: Boolean,
    sent: Boolean,
    created: { type : Date, default: Date.now() }
});
module.exports = mongoose.model('Notification', NotificationSchema);