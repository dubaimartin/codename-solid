'use strict';

/**
 * Author: Dubai Martin
 * Message model
 */

var mongoose = require('mongoose');
var path = require('path');

var MessageSchema = mongoose.Schema({
    owner: String,
    conversationId: String,
    name: String,
    text: String,
    profilePicture: String,
    created: { type : Date, default: Date.now }
});
module.exports = mongoose.model('Message', MessageSchema);