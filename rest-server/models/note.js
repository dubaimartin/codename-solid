'use strict';

/**
 * Author: Dubai Martin
 * Note model
 */

var mongoose = require('mongoose');
var path = require('path');

var NoteSchema = mongoose.Schema({
    title: String,
    description: String,
    owner: String,
    ownerName: String,
    attachments: [String],
    liked: [String],
    disliked: [String],
    privacy: String,
    sharedWith: [String],
    created: { type : Date, default: Date.now() },
    modified: { type : Date, default: Date.now() }
});
module.exports = mongoose.model('Note', NoteSchema);