'use strict';

/**
 * Author: Dubai Martin
 * User model
 * Not final version
 */

var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var userSchema = new Schema({
    firstName: String,
    lastName: String,
    email: String,
    password: String,
    dateOfBirth: String,
    profilePicture: String,
    metadata: {
        school: String,
        country: String,
        city: String,
        interests: String
    },
    following: [String],
    followers: [String],
    liked: [String],
    disliked: [String],
    conversations: [String],
    notifications: [String],
    favourites: [String]
});

var User = mongoose.model('User', userSchema);

module.exports = User;