'use strict';

/**
 * Author: Dubai Martin
 * Conversation model
 */

var mongoose = require('mongoose');
var path = require('path');

var ConversationSchema = mongoose.Schema({
    owner: String,
    targets: [String],
    messages: [String],
    profileImages: [String],
    names: [String],
    created: { type : Date, default: Date.now },
    updated: { type : Date, default: Date.now }
});
module.exports = mongoose.model('Conversation', ConversationSchema);