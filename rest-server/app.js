'use strict';

/**
 * Author: Dubai Martin
 * Main app.js file for NodeJS BE for my BSc thesis
 */

var express = require('express');
var bodyParser = require('body-parser');
var passport = require('passport');
var expressSession = require('express-session');
var cookieSession = require('cookie-session');
var cors = require('cors');
var register = require('./routes/register.js');
var upload = require('./routes/upload.js');
var note = require('./routes/note.js');
var login = require('./routes/login.js');
var users = require('./routes/users.js');
var notification = require('./routes/notification.js');
var conversation = require('./routes/conversation.js');
var db = require('./db.js');

var app = express();

app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Origin', req.headers.origin);
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept');
    if ('OPTIONS' == req.method) {
        res.send(200);
    } else {
        next();
    }
});

var server = app.listen(3333, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log('Example app listening at http://localhost:' + port);
});

//Required for login sessions to work
app.use(expressSession({ secret: 'solid1995', cookie: { maxAge: 3600000 } }));
app.use(passport.initialize());
app.use(passport.session());
app.use(cors({credentials: true, origin: true}));

app.use(register);
app.use(upload);
app.use(login);
app.use(note);
app.use(users);
app.use(notification);
app.use(conversation);

module.exports = app;
