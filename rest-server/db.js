'use strict';

/**
 * Author: Dubai Martin
 * Mongoose + mongoDB connection module
 */

var mongoose = require('mongoose');
var Grid = require('gridfs-stream');
var url = 'mongodb://localhost:27017/mongodb-solid';
// var url = 'mongodb://dubaimartin:dubaimartin@ds147905.mlab.com:47905/codename-solid';

var connection = mongoose.connect(url, function(err){
    if (err) throw err;
    else console.log('Connection established to', url);
});

Grid.mongo = mongoose.mongo;
var fs = Grid(connection);

module.exports = fs;
