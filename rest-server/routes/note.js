'use strict';

/**
 * Author: Dubai Martin
 * Note uploader route
 */

var express = require('express');
var bodyParser = require('body-parser');
var db = require('mongoose');
var Note = require('../models/note.js');
var File = require('../models/file.js');
var User = require('../models/user.js');

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.post('/note/save', function (req, res, callback) {
    var note = new Note(req.body);
    note.created = Date.now();
    note.owner = req.session.passport.user;

    note.save(function (err) {
        if (err) {
            console.error(err);
            return res.send(500);
        }
        return res.status(200).send(note);
    });
});

app.get('/notes/dashboard', function(req, res){
    var id = req.session.passport.user;

    User.find({_id: id}, function(err, data){
        if(err) {
            return res.send(500);
        }
        if(data[0]) {
            Note.find(
                {
                    $and: [
                        {
                            owner: { $in: data[0].following }
                        },
                        {
                            privacy: { $ne: 'private' }
                        },
                        {
                            $or: [
                                {
                                    privacy: { $eq: 'custom' },
                                    sharedWith: { $in: [ id ] }
                                },
                                {
                                    privacy : { $eq: 'public' }
                                }
                            ]
                        }
                    ]
                },
                function (err, notes) {
                    if(err) {
                        return res.send(500);
                    }
                    notes.sort(function(a,b){
                        // Turn your strings into dates, and then subtract them
                        // to get a value that is either negative, positive, or zero.
                        return new Date(b.date) - new Date(a.date);
                    });
                    return res.send({notes: notes, following: data[0].following});
                })
        } else {
            return res.send({notes: [], following: []});
        }
    });
});

app.put('/note/:noteid', function (req, res) {
    req.body.modified = Date.now();

    Note.update({_id: req.params.noteid}, req.body)
        .exec(function (err, result) {
            if(err) {
                return res.send(500);
            }
            return res.json({error: err, result: result});
        });
});

app.get('/notes', function (req, res) {
    Note.find({owner: req.session.passport.user})
        .exec(function (err, result) {
            if(err) {
                return res.send(500);
            }
            return res.json(result);
        })
});

app.get('/notes/shared', function (req, res) {
    Note.find({sharedWith: req.session.passport.user})
        .exec(function (err, result) {
            return res.json(result);
        })
});

app.get('/note/:noteid/attachments', function (req, res) {
    File.find({noteid: req.params.noteid}, function (err, files) {
        if (err) {
            return res.send(err);
        }
        return res.send(files);
    })
});

app.delete('/note/:noteid', function (req, res) {
    Note.find({_id: req.params.noteid})
        .remove(function (err, result) {
            if (err) {
                return res.send(500);
            }
            return res.send(200);
        })
});

app.get('/note/favourites', function (req, res) {
    User.find({_id: req.session.passport.user}, function (err, user) {
        if (err) {
            return res.send(err);
        }
        Note.find({_id: {$in: user[0].favourites} })
            .exec(function (err, notes) {
                if (err) {
                    return res.send(err);
                }
                return res.status(200).json(notes);
            })
    })
});

app.put('/note/:noteid/favourite/add', function (req, res) {
    User.update({_id: req.session.passport.user},
        {
            $push: {favourites: req.params.noteid}
        })
        .exec(function (err, result) {
            if(err) {
                return res.send(500);
            }
            return res.json(result);
        })
});

app.delete('/note/:noteid/favourite/remove', function (req, res) {
    User.update({_id: req.session.passport.user},
        {
            $pull: {favourites: req.params.noteid}
        })
        .exec(function (err, result) {
            if(err) {
                return res.send(500);
            }
            return res.json(result);
        })
});

app.put('/note/:noteid/like', function (req, res) {
    User.find({_id: req.session.passport.user}, function(err, result) {
        if(result[0].liked.indexOf(req.params.noteid) < 0) {
            User.update({_id: req.session.passport.user},
                {
                    $pull: {disliked: req.params.noteid},
                    $push: {liked: req.params.noteid}
                })
                .exec(function (err, result) {
                    if(err) {
                        return res.send(500);
                    }
                    Note.update({_id: req.params.noteid}, {
                        $pull: {disliked: req.session.passport.user},
                        $push: {liked: req.session.passport.user}
                    })
                        .exec(function (err, result) {
                            if(err) {
                                return res.send(500);
                            }
                            return res.json({error: err, userid: req.session.passport.user, action: 'add'});
                        });
                })
        } else {
            User.update({_id: req.session.passport.user},
                {
                    $pull: {
                        disliked: req.params.noteid,
                        liked: req.params.noteid
                    }
                })
                .exec(function (err, result) {
                    if(err) {
                        return res.send(500);
                    }
                    Note.update({_id: req.params.noteid}, {
                        $pull: {
                            liked: req.session.passport.user,
                            disliked: req.session.passport.user
                        }
                    })
                        .exec(function (err, result) {
                            if(err) {
                                return res.send(500);
                            }
                            return res.json({error: err, userid: req.session.passport.user, action: 'remove'});
                        });
                })
        }
    })
});

app.put('/note/:noteid/dislike', function (req, res) {
    User.find({_id: req.session.passport.user}, function(err, result) {
        if(result[0].disliked.indexOf(req.params.noteid) < 0) {
            User.update({_id: req.session.passport.user},
                {
                    $pull: {liked: req.params.noteid},
                    $push: {disliked: req.params.noteid}
                })
                .exec(function (err, result) {
                    if(err) {
                        return res.send(500);
                    }
                    Note.update({_id: req.params.noteid}, {
                        $pull: {liked: req.session.passport.user},
                        $push: {disliked: req.session.passport.user}
                    })
                        .exec(function (err, result) {
                            if(err) {
                                return res.send(500);
                            }
                            return res.json({error: err, userid: req.session.passport.user, action: 'add'});
                        });
                })
        } else {
            User.update({_id: req.session.passport.user},
                {
                    $pull: {
                        disliked: req.params.noteid,
                        liked: req.params.noteid}
                })
                .exec(function (err, result) {
                    if(err) {
                        return res.send(500);
                    }
                    Note.update({_id: req.params.noteid}, {
                        $pull: {
                            liked: req.session.passport.user,
                            disliked: req.session.passport.user
                        }
                    })
                        .exec(function (err, result) {
                            if(err) {
                                return res.send(500);
                            }
                            return res.json({error: err, userid: req.session.passport.user, action: 'remove'});
                        });
                })
        }
    })
});

app.get('/notes/:name', function (req, res) {
    var name = req.params.name;
    Note.find(
        {
            $and: [
                {
                    $or: [
                        {
                            title: {"$regex": name, "$options": "i"}
                        },
                        {
                            description: {"$regex": name, "$options": "i"}
                        }
                    ]
                },
                {
                    owner: { $ne: req.session.passport.user }
                },
                {
                    privacy: { $ne: 'private' }
                },
                {
                    $or: [
                        {
                            privacy: { $eq: 'custom' },
                            sharedWith: { $in: [req.session.passport.user] }
                        },
                        {
                            privacy : { $eq: 'public' }
                        }
                    ]
                }
            ]
        },
        function (err, data) {
            if(err) {
                return res.send(500);
            }
            return res.send(data);
        })
});

module.exports = app;
