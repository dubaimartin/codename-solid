'use strict';

/**
 * Author: Dubai Martin
 * Users route
 */

var express = require('express');
var bodyParser = require('body-parser');
var db = require('mongoose');
var Users = require('../models/user.js');

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.put('/users/:id/follow', function (req, res) {
    var id = req.params.id;
    var current = req.session.passport.user;
    Users.update({_id: current},
        {
            $push: {following: id}
        },
        function(err, result) {
            if(err) {
                return res.send(500);
            }
            Users.update({_id: id},
                {
                    $push: {followers: current}
                }, function(err, result){
                    if(err) {
                        return res.send(500);
                    }
                    return res.send(200);
                })
        })
});

app.put('/user/update', function(req, res){
    var id = req.session.passport.user;

    Users.update({_id: id}, req.body.person, function(err, result){
        if(err) {
            return res.send(500);
        }
        return res.send(200);
    })
});

app.put('/users/:id/unfollow', function (req, res) {
    var id = req.params.id;
    var current = req.session.passport.user;
    Users.update({_id: current},
        {
            $pull: {following: id}
        },
        function(err, result) {
            if(err) {
                return res.send(500);
            }
            Users.update({_id: id},
                {
                    $pull: {followers: current}
                }, function(err, result){
                    if(err) {
                        return res.send(500);
                    }
                    return res.send(200);
                })
        })
});

app.get('/users/:id/checkfollow', function (req, res) {
    var id = req.params.id;
    var current = req.session.passport.user;
    Users.find({_id: current, following: {$in: [id]}},
        function(err, result) {
            if(err) {
                return res.send(500);
            }
            if(result[0]) {
                return res.status(200).send({following: true});
            } else {
                return res.status(200).send({following: false});
            }
        })
});

app.get('/users/find/:name', function (req, res) {
    var name = req.params.name;
    Users.find(
        {
            $and: [
                {
                    $or: [
                        {
                            firstName: {"$regex": name, "$options": "i"}
                        },
                        {
                            lastName: {"$regex": name, "$options": "i"}
                        }
                    ]
                },
                {
                    _id: { $ne: req.session.passport.user }
                }
            ]
        },
        function (err, data) {
            if(err) {
                return res.send(500);
            }
            return res.send(data);
        })
});

app.post('/users/find/id', function (req, res) {
    var ids = req.body.ids;
    Users.find(
        {
            _id: {$in: ids}
        },
        function (err, data) {
            if(err) {
                return res.send(500);
            }
            return res.status(200).send(data);
        })
});

module.exports = app;
