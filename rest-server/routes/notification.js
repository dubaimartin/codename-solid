'use strict';

/**
 * Author: Dubai Martin
 * Notification service route
 */

var express = require('express');
var bodyParser = require('body-parser');
var db = require('mongoose');
var Note = require('../models/note.js');
var Notification = require('../models/notification.js');
var User = require('../models/user.js');

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

/*
 Request: {
 type: noteShare
 username: String,
 userIds: [String]
 }
 */
app.post('/notifications/note/share', function (req, res, callback) {
    if (req.body.type === 'noteShare' || req.body.type === 'noteShareRemove') {
        for (var i = 0; i < req.body.userIds.length; i++) {
            var notification = new Notification();

            notification.owner = req.body.userIds[i];
            notification.created = Date.now();
            notification.type = 'interaction';
            req.body.type === 'noteShare' ? notification.title = 'New shared Note' : notification.title = 'Shared Note access removed';
            req.body.type === 'noteShare' ?
                notification.message = req.body.username + ' shared a Note with You!' : notification.message = req.body.username + ' is no longer sharing a Note with You!';
            notification.action = 'viewSharedNotes';
            notification.read = false;
            notification.sent = false;

            if(req.body.type === 'noteShare'){
                addNotification(req, res, callback, notification);
            } else {
                addNoteShareRemoveNotification(req, res, callback, notification);
            }
        }
    }
});

app.post('/notifications/conversation', function (req, res, callback) {
    for (var i = 0; i < req.body.userIds.length; i++) {
        var notification = new Notification();

        notification.owner = req.body.userIds[i];
        notification.created = Date.now();
        notification.type = 'interaction';
        notification.title = 'New conversation';
        notification.message = req.body.username + ' started a conversation with You!';
        notification.read = false;
        notification.sent = false;

        addNotification(req, res, callback, notification);
    }
});

app.get('/notifications', function (req, res, callback) {
    Notification.find({owner: req.session.passport.user})
        .exec(function (err, result) {
            if (err) {
                console.error(err);
                return res.status(500).send(err);
            }
            return res.status(200).send(result);
        })
});

app.get('/notifications/new', function (req, res, callback) {
    Notification.find({owner: req.session.passport.user, read: false})
        .exec(function (err, result) {
            return res.json(result);
        })
});

app.put('/notifications/:id/sent', function(req, res, callback) {
    Notification.update({_id: req.params.id}, {sent: true})
        .exec(function (err, result) {
            if (err) {
                console.error(err);
                return res.status(500).send(err);
            }
            return res.status(200);
        });
});

function addNotification(req, res, callback, notification) {
    notification.save(function (err) {
        if (err) {
            console.error(err);
            return res.status(500).send(err);
        }
        User.update(
            {_id: {$in: req.body.userIds}},
            {$push: {notifications: notification._id}},
            {multi: true},
            function (err) {
                if (err) {
                    console.error(err);
                    return res.status(500).send(err);
                }
                return res.status(200);
            })
    });
}

function addNoteShareRemoveNotification(req, res, callback, notification) {
    notification.save(function (err) {
        if (err) {
            console.error(err);
            return res.status(500).send(err);
        }
        User.update(
            {_id: {$in: req.body.userIds}},
            {$pull: {notifications: notification._id}},
            {multi: true},
            function (err) {
                if (err) {
                    console.error(err);
                    return res.status(500).send(err);
                }
                return res.status(200);
            })
    });
}

module.exports = app;
