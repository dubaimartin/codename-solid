'use strict';

/**
 * Author: Dubai Martin
 */

var express = require('express');
var multer = require('multer');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var db = require('../db.js');
var Note = require('../models/note.js');
var User = require('../models/user.js');
var File = require('../models/file.js');
var fs = require('fs');
var path = require('path');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.post('/note/upload/:noteid', function (req, res) {
    var id = req.session.passport.user;
    var upload = multer({dest: './uploads/' + id}).single('file');

    upload(req, res, function (err) {
        if (err) {
            return res.status(500);
        }
        return saveFile(req, res);
    });
});

app.put('/note/upload/:id', function (req, res) {
    File.update({name: req.params.id}, {
        newName: req.body.newName,
        modified: new Date()
    })
        .exec(function (err, result) {
            return res.json({error: err, result: result});
        });
});

app.post('/user/upload/profilePicture', function (req, res) {
    var id = req.session.passport.user;
    var upload = multer({dest: './uploads/' + id}).single('file');

    upload(req, res, function (err) {
        if (err) {
            return res.status(500);
        }
        return saveFile(req, res, 'user');
    });
});

function saveFile(req, res, type) {
    var file = new File({
        uploader: req.session.passport.user,
        noteid: req.params.noteid,
        name: req.file.filename,
        newName: req.file.originalname,
        type: req.file.mimetype,
        destination: req.file.destination,
        path: req.file.path,
        privacy: 'public',
        size: req.file.size,
        created: Date.now(),
        lastModifiedDate: Date.now()
    });

    file.save(function (err) {
        if (err) {
            res.status(500);
            return res.end();
        }
        if (type === 'user') {
            var url = 'http://localhost:3333/get/' + file.uploader + '/' + file.name;
            User.update({ _id: req.session.passport.user }, { profilePicture: url })
                .exec(function (err, result) {
                    if (err) {
                        console.error(err);
                        return res.status(500).send(err);
                    }
                    return res.status(200).json({url: url});
                });
        } else {
            return res.status(200).send(file);
        }
    });
}

app.delete('/note/upload/:id', function (req, res) {
    var id = req.session.passport.user;
    return fs.unlink('./uploads/' + id + '/' + req.params.id, function (err) {
        if (err) {
            res.status(500);
            return res.send(500);
        }
        return deleteFileFromCollection(res, req, req.params.id);

    });
});

function deleteFileFromCollection(res, req, id) {
    File.find({name: id}).remove(function (err) {
        if (err) {
            res.status(500);
            return false;
        }
        res.status(200);
        return res.end();
    });
}

app.get('/get/:uploader/:id', function (req, res) {
    File.find({name: req.params.id}).exec(function (err, result) {
        if (err) {
            return res.send(500);
        }
        var file = '.\\uploads\\' + req.params.uploader + '\\' + req.params.id;
        return res.download(file); // Set disposition and send it.
    });
});

app.get('/get/pdf/:uploader/:id', function (req, res) {
    File.find({name: req.params.id}).exec(function (err, result) {
        if (err) {
            return res.send(500);
        }
        var file = '.\\uploads\\' + req.params.uploader + '\\' + req.params.id;

        res.setHeader('Content-Type', 'application/pdf');
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Content-Disposition', 'inline; filename="filename.pdf"');
        return res.sendfile(file);
    });
});

module.exports = app;
