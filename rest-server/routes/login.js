var User = require('../models/user.js');
var express = require('express');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var bcrypt = require('bcrypt-nodejs');

var app = express();

passport.serializeUser(function(user, done) {
    done(null, user[0]._id);
});

passport.deserializeUser(function(id, done) {
    User.find({_id: id}).exec(function(err, user) {
        done(err, user);
    })
});

passport.use('local-signup', new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
    },
    function(req, username, password, done) {
        User.find({email: username}).exec(function(err, result){
            if(result.length == 1){
                if(bcrypt.compareSync(password, result[0].password)) {
                    return done(null, result, null);
                } else {
                    return done(null, false, {error: 'invalid password'});
                }
            } else {
                return done(null, false, {error: 'invalid username'});

            }
        });
    }
));

app.post('/login', function(req, res, next) {
    passport.authenticate('local-signup', function(err, user, info) {
        if (err) {
            return next(err); // will generate a 500 error
        }
        if (!user) {
            return res.status(401).send({ success: false, message: info.error });
        }
        req.login(user, function(err){
            if(err){
                return next(err);
            }
            return res.status(200).send({ success: false, user: req.user[0] });
        });
    })(req, res, next);
});

app.get('/login', function(req, res){
    if(req.user){
        return res.status(200).send(true);
    } else {
        return res.status(401).send(false);
    }
});

app.get('/logout', function(req, res){
    req.logout();
    res.sendStatus(200);
});

module.exports = app;