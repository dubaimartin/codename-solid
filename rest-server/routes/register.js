'use strict';

/**
 * Author: Dubai Martin
 * User registration route
 * Task: Error checking on /register POST request,
 * and provide response according to error type / success
 */

var express = require('express');
var bodyParser = require('body-parser');
var mkdirp = require('mkdirp');
var bcrypt = require('bcrypt-nodejs');

var User = require('../models/user.js');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.post('/register', function (req, res) {
    var user = new User(req.body);
    mkdirp('./uploads/' + user._id + '/', function(err) {
    });

    user.password = bcrypt.hashSync(user.password);
    // user.profilePicture = 'https://d8wjjpr0e320t.cloudfront.net/projects/arena/ui/default_profile_photo.png';
    checkErrors(req, res, user, registerUser);
});

function registerUser(req, res, user){
    user.save(function(err){
       if (err) {
            console.error(err);
            return res.status(500).send(err);
        }
        //New user successfully saved
        return res.sendStatus(201);
    });
};

function checkErrors(req, res, user, callback){
    var properties = [ 'firstName', 'lastName', 'email', 'password', 'dateOfBirth' ];
    var missingFields = [];

    for(var i = 0; i < properties.length; i++){
        if(!req.body.hasOwnProperty(properties[i])){
            missingFields.push(properties[i]);
        }
    }

    if(missingFields.length > 0) {
        return res.status(400).send({"missingFields" : missingFields});
    }
        
    var criteria = {
        email: req.body.email
    };

    User.find(criteria).exec(function(err, result){
        if(result.length > 0){
            return res.status(409).send('user already exists');
        }

        return callback(req, res, user);
    });
    
};

module.exports = app;
