'use strict';

/**
 * Author: Dubai Martin
 * Conversation route
 */

var express = require('express');
var bodyParser = require('body-parser');
var db = require('mongoose');
var User = require('../models/user.js');
var Message = require('../models/message.js');
var Conversation = require('../models/conversation.js');

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.get('/conversation/getAll', function(req, res, callback) {
    var results = [];
    Conversation.find({owner: req.session.passport.user })
        .exec(function (err, result) {
            if (err) {
                console.error(err);
                return res.status(500).send(err);
            }
            if(result) {
                results = result;
            }
            Conversation.find({targets: { $in: [req.session.passport.user] } })
                .exec(function (err, result2) {
                    if (err) {
                        console.error(err);
                        return res.status(500).send(err);
                    }
                    if(result2) {
                        for(var i = 0; i < result2.length; i++) {
                            results.push(result2[i]);
                        }
                    }

                    return res.status(200).send(results);
                })
        })
});

app.get('/conversation/:id/get', function(req, res, callback) {
    Conversation.find({_id: req.params.id})
        .exec(function (err, result) {
            if (err) {
                console.error(err);
                return res.status(500).send(err);
            }
            Message.find({_id: {$in: result[0].messages} })
                .exec(function (err, msg) {
                    if (err) {
                        console.error(err);
                        return res.status(500).send(err);
                    }
                    return res.status(200).send({
                        conversation: result,
                        messages: msg
                    });
                })
        })
});

/*
{
    targets: ['userid']
}
 */
app.post('/conversation/new', function (req, res, callback) {
    var conversation = new Conversation();
    conversation.targets = req.body.targets;
    conversation.owner = req.session.passport.user;
    conversation.names = req.body.names;
    conversation.profileImages = req.body.profilePictures;

    var userIds = req.body.targets;
    userIds.push(req.session.passport.user);

    conversation.save(function (err) {
        if (err) {
            console.error(err);
            return res.sendStatus(500);
        }
        User.update(
            {_id: {$in: userIds}},
            {$push: {conversations: conversation._id}},
            {multi: true},
            function (err) {
                if (err) {
                    console.error(err);
                    return res.status(500).send(err);
                }
                return res.status(200).send(conversation);
            });
    });
});

/*
{
    message: 'string',
}
 */
app.post('/conversation/:id/update', function (req, res, callback) {
    var id = req.params.id;

    User.find({_id: req.session.passport.user})
        .exec(function(err, result) {
            if (err) {
                console.error(err);
                return res.send(500);
            }

            var message = new Message();
            message.text = req.body.message;
            message.owner = req.session.passport.user;
            message.name = req.body.name;
            message.conversationId = id;
            message.profilePicture = result[0].profilePicture;

            message.save(function (err) {
                if (err) {
                    console.error(err);
                    return res.send(500);
                }

                Conversation.update(
                    {_id: id},
                    {$push: {messages: message._id}}, {updated: Date.now()},
                    function (err) {

                        if (err) {
                            console.error(err);
                            return res.status(500).send(err);
                        }
                        return res.status(200).send(message);
                    });
            });
        })
});

app.delete('/conversation/:id/delete', function (req, res, callback) {
    var id = req.params.id;
    var userIds = req.body.targets;
    userIds.push(req.session.passport.user);

    Conversation.find({_id: id})
        .remove(function (err) {
            if (err) {
                return res.send(500);
            }
            User.update(
                {_id: {$in: userIds}},
                {$pull: {conversations: id}},
                {multi: true},
                function (err) {
                    if (err) {
                        console.error(err);
                        return res.status(500).send(err);
                    }
                    return res.status(200);
                });
        })
});

module.exports = app;
